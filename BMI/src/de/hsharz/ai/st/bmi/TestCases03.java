package de.hsharz.ai.st.bmi;

import static org.junit.Assert.assertEquals;

public class TestCases03 {

	public double calculateBMI(int weight, double height) {
		if (height <= 0 || height > 3 || weight <= 0 || weight > 1000) {
			return -1;
		}
		try {
			return height / Math.multiplyExact(weight, weight);
		} catch (ArithmeticException e) {
			return -1;
		}
	}

	private void test(double height, int weight, double result) {
		assertEquals(result, calculateBMI(weight, height), 0);
	}

	@org.junit.Test
	public void testNormal01() {
		test(2, 100, 0.0002);
	}

	@org.junit.Test
	public void testNormal02() {
		test(2, 80, 0.0003125);
	}

	@org.junit.Test
	public void testNormal03() {
		test(1.5, 50, 0.0006);
	}

	@org.junit.Test
	public void testNegativeWeight() {
		test(1, -10, -1);
	}

	@org.junit.Test
	public void testZeroWeight() {
		test(1, 0, -1);
	}

	@org.junit.Test
	public void testMaximumWeight() {
		test(2, 1000, 0.000002);
	}

	@org.junit.Test
	public void testTooLargeWeight() {
		test(Integer.MAX_VALUE, (int) Math.ceil(Math.sqrt(Integer.MAX_VALUE)), -1);
	}

	@org.junit.Test
	public void testNegativeHeight() {
		test(-1, 100, -1);
	}

	@org.junit.Test
	public void testZeroHeight() {
		test(0, 100, -1);
	}

	@org.junit.Test
	public void testMinimumHeight01() {
		test(Double.MIN_VALUE, 1, Double.MIN_VALUE);
	}

	@org.junit.Test
	public void testMinimumHeight02() {
		test(Double.MIN_VALUE, 2, 0);
	}

	@org.junit.Test
	public void testMaximumHeight() {
		test(3, 1, 3);
	}

}
